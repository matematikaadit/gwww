<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>
  <xsl:template match="/">
    <html>
     <head>
       <meta name="viewport" content="width=device-width, initial-scale=1"/>
       <title><xsl:value-of select="$title"/></title>
<!-- It's frustating that autoindent is all over the place in emacs. -->
<style>
body {
  font-size: 16px;
  font-family: serif;

  max-width: 50em;
  margin: 0;
  padding: 1em 1em 4em;
}
table {
  display: block;
  overflow-x: auto;

  table-layout: auto;
  border-collapse: collapse;
  border-spacing: 0;
}
td, th {
  padding: 2px 0.5em;
  border: 1px solid black;
  white-space: nowrap;
}
td {
  text-align: right;
}
th {
  border-bottom: 2px solid black;
}
td:nth-child(3), td:nth-child(4) {
  text-align: left;
}
</style>
<!-- moral lesson: just deal with it -->
      </head>
      <body>
        <h1><xsl:value-of select="$title"/></h1>
        <p>Updated at: <span class="time"><xsl:value-of select="$time"/></span> UTC</p>
        <xsl:apply-templates select="$table"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="node()">
    <xsl:copy>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="table">
    <xsl:copy>
      <xsl:apply-templates select="tbody/tr[1]"/>
      <xsl:apply-templates select="tbody/tr[position()>1]">
	<xsl:sort select="td[1]" order="descending" data-type="number"/>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="tr">
    <xsl:copy>
      <xsl:if test="not(concat(td[1], td[2], td[3], td[4])='')">
	<xsl:apply-templates select="td[1]"/>
	<xsl:apply-templates select="td[3]"/>
	<xsl:apply-templates select="td[2]"/>
	<xsl:apply-templates select="td[4]"/>
      </xsl:if>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="tr[1]/td">
    <th><xsl:apply-templates/></th>
  </xsl:template>
  
  <xsl:template match="tr[1]/td/text()[.='Chapter #']">
    <xsl:text>#</xsl:text>
  </xsl:template>

  <xsl:template match="tr[1]/td/text()[.='Word Count']">
    <xsl:text>WC</xsl:text>
  </xsl:template>

  <xsl:variable name="title">
    <xsl:text>Worth the Candle - Word Count</xsl:text>
  </xsl:variable>

  <xsl:variable name="table" select="//*[@id='0']/div/table"/>
</xsl:stylesheet>
