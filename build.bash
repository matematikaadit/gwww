#!/usr/bin/env bash
set -euxo pipefail

target_url='https://docs.google.com/spreadsheets/d/1PaLrwVYgxp_SYHtkred7ybpSJPHL88lf4zB0zMKmk1E/htmlview'
user_agent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36'
accept='text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
curl_opts=(
    "$target_url"
    -H "authority: docs.google.com"
    -H "upgrade-insecure-requests: 1"
    -H "user-agent: $user_agent"
    -H "accept: $accept"
    -H "accept-encoding: gzip, deflate, br"
    -H "accept-language: en-US,en;q=0.9,id;q=0.8,ja;q=0.7,ru;q=0.6"
    --compressed
)
xsltproc_opts=(
    --html
    --stringparam time "$(date --utc +"%H:%M")"
    --output public/index.html
    clean.xsl
    -
)

[[ -d public ]] && rm -rf public
mkdir -p public

curl "${curl_opts[@]}" | xsltproc "${xsltproc_opts[@]}"
